# How to:
### Prerequisites
A Mac OS X or a Windows 7, 8, or 10 development machine.

### The minimum versions required by Mobile SDK 8.0 are:
 * Node.js: **latest**
 * npm **3.10** (installed with node.js)
 * Git command line: **latest**

# Mac: 
## First of all set up Mobile SDK dev tools as described below or under next [link](https://trailhead.salesforce.com/content/learn/projects/mobilesdk_setup_dev_tools)

1. **Installing NodeJS with npm** 
	- Go to [nodejs.org](nodejs.org).
	- Download and run the latest LTS installer for your operating system.
2. **Git Command Line**
	- Go to [git-scm.com/downloads](git-scm.com/downloads).
	- Under Downloads, click the icon for your operating system.
	- Run the installer.
3. **Install CocoaPods**
	- 
	Using terminal run: 
	```
	sudo gem install cocoapods
	```
4. **Install forcereact**
	- 
	Using terminal run: 
	```
	sudo npm install -g forcereact
	```
5. **Clone current project or create new one by using** ``` forcereact create ```
6. **Change settings in next files under folder** ```<appFolder>/ios<appName>/```
	1. in file **Info.plist** change property of **SFDCOAuthLoginHost** if needed
	2. in file **bootconfig.plist** change properties of **remoteAccessConsumerKey** and **oauthRedirectURI** to your own
	
7. Run project
	In Mac OS:
	```
		npm start
	```
	and in Windows
	```
	npm run-script start-windows
	```
	
	When the command line reports “Loading dependency graph, done,” you can run the app from within your development environment.
	
	
# Community
	1. Create community
	2. Create User
	3. Configure community:
		- Go to Community administration page
		- Add user to community members by adding of it's profile or perm.set
		- MArk checkbox Allow internal users to log in directly to the community as checked under Login & Registration tab
		- Optionally configure Login screen under Login & Registration tab
